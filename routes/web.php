<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@welcome')->name('welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/admin', 'Admin@index')->name('admin'); // Admin Page
Route::post('/admin', 'Admin@create'); // Admin Page with post data to create a hotel
Route::get('/hotel', 'HotelController@index')->name('hotel'); // Hotel Single page
Route::post('/hotel', 'HotelController@create'); // Hotel Single Page with post data to create a hotel
