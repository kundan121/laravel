<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hotel;
use App\Comments;
use Illuminate\Support\Facades\Auth;
class HotelController extends Controller
{
    /**
     * Show all comments list of a hotel
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hotel_id = $_GET['id']; // Get hotel id from page url
        $hotels = Hotel::where('id', $hotel_id)->get(); // Fetch the current hotel details
        $comments = Comments::where('hotel_id', $hotel_id)->get(); // Fetch all comments of the current hotel
        if(Auth::user()){
            $user_type = Auth::user()->type;
        }
        else{
            $user_type = "";
        }
        return view('hotel',['hotel'=>$hotels , 'comments'=>$comments , 'user_type'=>$user_type]);
    }

    /**
     * Save comment of a hotel
     *
     * @return \Illuminate\Http\Response
     */
    protected function create(Request $request){
        $comment = new Comments;
        $comment->name = $request->comment;
        $comment->hotel_id = $request->hotel_id;
        $comment->user_id = Auth::user()->id;
        $comment->save();
        return redirect('/hotel?id='.$request->hotel_id.'&created');
    }
}
