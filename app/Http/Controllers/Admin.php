<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hotel;
use Illuminate\Support\Facades\Auth;
class Admin extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Admin page to create the hotel (only admin can access the page)
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check()){
            if(Auth::user()->type=="admin"){
                return view('admin');
            }
        }
        return redirect('/index.php?not_admin');
    }

    /**
     * Save the hotel data
     *
     * @return \Illuminate\Http\Response
     */
    protected function create(Request $request)
    {
        $hotel = new Hotel;
        $hotel->name = $request->name;
        $hotel->description = $request->description;
        $hotel->save();
        return redirect('/admin?created');
    }
}
