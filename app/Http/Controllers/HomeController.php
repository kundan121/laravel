<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hotel;
class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show all hotels list
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome()
    {
        $hotels = Hotel::get();
        return view('welcome',['hotels'=>$hotels]);
    }


}
