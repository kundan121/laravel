@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Hotels List</div>

                    <div class="panel-body">
                        @foreach($hotel as $p)
                            <h3>{{ $p['name'] }}</h3> {{ $p['description'] }}
                        @endforeach
                        <br>
                        Comments below
                        <br><br>
                        @foreach($comments as $c)
                            {{ $c['name'] }}<br>
                        @endforeach
                            @if($user_type=="admin" || $user_type=="default")
                                <form class="form-horizontal" method="POST" action="{{ route('hotel') }}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="hotel_id" value="{{ $_GET['id'] }}">
                                    <textarea name="comment" placeholder="Comment Details"></textarea>
                                    <input type="submit" value="Comment Here">
                                </form>
                            @else
                                <a href="/login">Please login to comment</a>
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
