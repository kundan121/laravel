@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create Hotel</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('admin') }}">
                            {{ csrf_field() }}
                            <input type="text" name="name" placeholder="Hotel name">
                            <textarea name="description" placeholder="Details"></textarea>
                            <input type="submit" value="Save Hotel">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
