@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if(isset($_GET['not_admin']))
                    <h3>Please login with admin to access that page</h3>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Hotels List</div>

                    <div class="panel-body">
                        @foreach($hotels as $p)
                            <a href="/hotel?id={{ $p['id'] }}"> {{ $p['name'] }} , {{ $p['description'] }}</a>
                            <br><br>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
